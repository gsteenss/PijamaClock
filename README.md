# PijamaClock
Python script for a Raspberry Pi alarm clock using the Sense HAT module

![40f44c3123e49508](/uploads/9d9448d94eb7eb8101ae48839294e99a/40f44c3123e49508.jpg)

# requirements

* Raspberry Pi
* Sense HAT module

If you don't have the Sense HAT module, you can try out the script using the sense-HAT emulator, 
for more details see: https://www.raspberrypi.org/blog/desktop-sense-hat-emulator/


# installation

to run the Python script make sure you have the Sense HAT & Python Image Library installed.

on Rasbian try: 
<i>
sudo apt-get install python-sense-hat python-pil -y
</i>

to start the clock automatically after booting up your Pi you can add the script to your /etc/rc.local like this:

<i>  
python /home/pi/PijamaClock/PijamaClock.py &
</i>

# features

* LED clock with 1 digit for hour (12h) and a clockface circle for representing minutes (in 3 minute intervals)
* display rotation according to orientation detected by the integrated Sense HAT gyroscope
* LED is switched to low light mode between 6pm - 6am
* joystick button can be pressed to show current ambient temperature (between 0 - 99 degrees Celsius)
* basic temperature compensation for CPU heat affecting Sense HAT temperature sensors
* scrolls an error message when temperature measurements are invalid
* added basic rainbow effect for wake-up alarm (set to 6am by default, make sure to change this if that's too early for you)
* added quick and dirty way to configure variables in PijamaClock.conf file

# TODO

* implementing joystick button pressing to switch lowlight mode on/off for better readability 
* ...
