from PIL import Image
from random import randint
import time
import os
# use Hardware Sense HAT or Sense HAT emulator (see: https://sense-emu.readthedocs.io/en/v1.0/install.html)
try:
	from sense_hat import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED
	hardware_sense_hat=True
except:
	from sense_emu import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED
	hardware_sense_hat=False

# make sure to change current dir to our script's path
os.chdir(os.path.dirname(os.path.realpath(__file__)))

s = SenseHat()
s.clear()

s.low_light = True
nighttime=False

green = (0, 255, 0)
greenlow = (0, 8, 0)
yellow = (255, 255, 0)
blue = (0, 0, 255)
bluelow = (0, 0, 8)
red = (255, 0, 0)
white = (255,255,255)
whitelow = (8,8,8)
nothing = (0,0,0)
pink = (255,105, 180)
hour_color=blue
min_color=white

# pixels for the hour & clockface circle which displays 3-minute intervals
clockface=[(),(0,4),(0,5),(1,6),(2,7),(3,7),(4,7),(5,7),(6,6),(7,5),(7,4),(7,3),(7,2),(6,1),(5,0),(4,0),(3,0),(2,0),(1,1),(0,2),(0,3)]
one=[(2,3),(3,2),(4,1),(4,2),(4,3),(4,4),(4,5),(4,6),(5,6),(3,6),(2,6)]
two=[(2,2),(3,1),(4,1),(5,2),(5,3),(4,4),(3,5),(4,6),(5,6),(3,6),(2,6)]
three1=[(2,2),(3,1),(4,1),(5,2),(5,3),(4,4),(5,5),(4,6),(3,6),(2,5)]
three2=[(2,2),(3,1),(4,1),(5,2),(5,3),(4,4),(5,5),(4,6),(3,6),(2,6)]
three=[(2,1),(3,1),(4,1),(5,1),(5,2),(5,3),(5,4),(5,5),(5,6),(3,4),(4,4),(2,6),(3,6),(4,6),(5,6)]
four=[(2,1),(2,2),(2,3),(2,4),(5,4),(5,5),(5,6),(5,2),(5,3),(3,4),(4,4),(5,6)]
five=[(2,1),(3,1),(4,1),(5,1),(2,2),(2,3),(3,3),(4,3),(5,4),(5,5),(4,6),(3,6),(2,5)]
six=[(3,1),(4,1),(2,2),(2,3),(2,4),(3,3),(4,3),(5,4),(5,5),(4,6),(3,6),(2,5)]
seven=[(2,1),(3,1),(4,1),(5,1),(5,2),(4,3),(4,4),(3,5),(3,6)]
eight=[(2,1),(3,1),(4,1),(5,1),(5,2),(5,3),(5,4),(5,5),(2,2),(2,3),(2,4),(2,5),(5,6),(3,4),(4,4),(2,6),(3,6),(4,6),(5,6)]
nine=[(3,1),(4,1),(2,2),(2,3),(5,2),(5,3),(5,4),(5,5),(3,4),(4,4),(2,6),(3,6),(4,6)]
ten=[(1,2),(2,1),(2,2),(2,3),(2,4),(2,5),(2,6),(5,1),(4,2),(4,3),(4,4),(4,5),(6,2),(6,3),(6,4),(6,5),(5,6)]
eleven=[(1,2),(2,1),(2,2),(2,3),(2,4),(2,5),(2,6),(4,2),(5,1),(5,2),(5,3),(5,4),(5,5),(5,6)]
twelve=[(1,2),(2,1),(2,2),(2,3),(2,4),(2,5),(2,6),(4,2),(5,1),(6,2),(6,3),(5,4),(4,5),(4,6),(5,6)]
numbers=(one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve)


#show rainbow a
def show_rainbow(animate=True):
  rainbow=Image.open("rainbow.png")
  s.set_pixels(list(rainbow.getdata()))
  
  while True:     
    
    time.sleep(0.1)    
    savepix=s.get_pixel(0,0)    
    s.set_pixel(0,0,s.get_pixel(7,7))
    
    for x in range(1,8):
      loop=x+1;
      prevpix=savepix
      savepix=s.get_pixel(x,0)
      for pix in range(0,loop):  
	s.set_pixel((x-pix)%8,(pix)%8,prevpix)
      
    for y in range(1,8):
      loop=8-y
      prevpix=savepix
      savepix=s.get_pixel(7,y)
      for pix in range(0,loop):  
	s.set_pixel((7-pix)%8,(y+pix)%8,prevpix)
	
    for event in s.stick.get_events():      
      if event.action == ACTION_RELEASED: 
        s.clear()
        return False
	

# get CPU temperature
def get_cpu_temp():
  if hardware_sense_hat: 
	  res = os.popen("vcgencmd measure_temp").readline()
  else:
	  res = "temp=25"
  t = float(res.replace("temp=","").replace("'C\n",""))
  return(t)


def set_lowlight(sense,lowlight=True):
    s.low_light = lowlight
    if lowlight:        
        min_color = whitelow
        hour_color = bluelow    
    else:
        min_color = white
        hour_color = blue    
        
    return (hour_color,min_color)

def update_time():    
    tm=time.localtime()
    hour=tm.tm_hour-1
    min=tm.tm_min
    sec=tm.tm_sec
    return (hour,min,sec)

def clock_time():
    t=update_time()
    hour=t[0]
    min=t[1]
    sec=t[2]
    pm=t[0]>12

    nighttime=False
    # nightime starts at 6pm ends at 6am
    if hour>=17: nighttime=True
    if hour<6: nighttime=True

    # returns hour & minutes in 12h format and minutes in 3-minute intervals for showing the clockface
    clock_hour=hour%12
    clock_min=int(min/3)
            
    return (clock_hour,clock_min,nighttime,pm)

def set_hour(num,col):
    for pix in num:
        s.set_pixel(pix[0],pix[1],col)

# sets all corresponding clockface pixels based on minutes given 
def set_minutes(min,col):
  for x in range(0, min):
    if clockface[x]: s.set_pixel(clockface[x][1],clockface[x][0],col)
    
# sets the clockface pixel for a given minute only (progressive) 
def set_minute(min,col):
    if clockface[min]: s.set_pixel(clockface[min][1],clockface[min][0],col)


# set display rotation based on accelerometer info
def set_display_rotation():
  accel=s.get_accelerometer_raw()
  x = round(accel['x'], 0)
  y = round(accel['y'], 0)
  rot = 0
  if x == -1:
    rot=90
  elif y == -1:
    rot=180
  elif x == 1:
    rot=270
  s.set_rotation(rot)

# set hour to enable rainbow alarm function
rainbow_alarm=5

# quick and dirty way to configure any default variables via PijamaClock.conf
try:
  conf=open('PijamaClock.conf','r');
  for line in conf: exec(line);
except:
  print('PijamaClock.conf not found');  

# clock is not set yet
clock_hour=clock_min=0
update_hour=update_min=0
pm=False
# set led output to lowlight by default
(hour_color,min_color)=set_lowlight(s)

while True:

  set_display_rotation() 	  
  (update_hour,update_min,nighttime,pm)=clock_time()
  (hour_color,min_color)=set_lowlight(s,nighttime)
   
  # we only display something if hour or minutes have changed
  # but during nighttime we update the exact minute pixel to minimize light output
  if (clock_hour!=update_hour):
      clock_hour=update_hour
      s.clear()
      set_hour(numbers[clock_hour],hour_color)      
      
      # rainbow alarm will only wake up during nighttime mode and on the exact hour specified
      if pm==False and rainbow_alarm and nighttime and rainbow_alarm==update_hour and update_min==0:
	set_lowlight(s,False)
	show_rainbow()

    
  if (clock_min!=update_min):
      clock_min=update_min
      if nighttime:
	s.clear()
        set_hour(numbers[clock_hour],hour_color)      
      	set_minute(clock_min,min_color)
      else:
	set_minutes(clock_min,min_color)
 
  # optionally display the current temperature
  t = s.get_temperature()
  t_cpu = get_cpu_temp()
  # calculates the real temperature compensating for CPU heating  
  temp = int(t - ((t_cpu-t)/1.1))
  
  # but only if the joystick button is pressed
  for event in s.stick.get_events():      
      if event.action == ACTION_PRESSED: 
        s.clear()      
	if (temp<0 or temp >99):
		s.show_message("Temp error",text_colour=red,scroll_speed=0.05);
	else:
	        im = Image.new("RGB", (8, 8), "black")  
        	if (temp<10): 
            		digit=Image.open(str(temp)+".png")
			im.paste(digit,(2,0))
	        else:
            		digit_one=Image.open(str(temp/10)+".png")
            		digit_two=Image.open(str(temp%10)+".png")
            		im.paste(digit_one,(0,0))
            		im.paste(digit_two,(4,0))
        	s.set_pixels(list(im.getdata()))
      # TODO: implement swapping lowlight mode of leds when button is pressed
      #if  event.action == ACTION_PRESSED: 
          #if s.low_light: set_lowlight(s,False)
          #else: set_lowlight(s,True)              
      if event.action == ACTION_RELEASED: 
            s.clear()
            set_hour(numbers[update_hour],hour_color)
	    if nighttime:
            	set_minute(update_min,min_color)
            else: 
		set_minutes(update_min,min_color)

    
  # sleep a bit before looping again
  time.sleep(1.0001)
